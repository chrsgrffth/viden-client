# Viden
---

## Resources

- Vue
- Brunch
- Ionicons ([Docs](http://ionicons.com))


## Getting Started

1. `npm install`
2. Ensure the API server is running (if not connected to remote).
3. `npm start`