var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var Path       = require('path');
var fs         = require('fs');
var ejs        = require('ejs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static(Path.join(__dirname, '/public')));

// Settings.
var port = process.env.PORT || 3000;

// Namespace.
app.use('/', function(req, res) { 
  fs.readFile(Path.join(__dirname,'/public/index.html'), 'utf-8', function(err, content) {
    if (err) {
      res.end('Error occurred: ' + err);
      return;
    }
    renderedHtml = ejs.render(content, { 'apiUrl': process.env.API_URL || 'http://api.viden.dev/v1' });
    res.send(renderedHtml);
  });
});

app.listen(port);
console.log('Server listing on http://localhost:' + port);